#include <algorithm>
#include <iostream>
#include <cmath>
#include <GL/glut.h>
#include <GL/glu.h>
#include "linear_algebra.cpp"
#include <unistd.h>
#include <cstdlib>
#include <vector>

const int WINDOW_WIDTH = 900;
const int WINDOW_HEIGHT = 900;

struct Line {
    Point2 a;
    Point2 b;
};

void drawLine2(Point2 v, Point2 u)
{
    glBegin(GL_LINES);
    glVertex2f(v.x, v.y);
    glColor3f(1,1,1);
    glVertex2f(u.x, u.y);
    glEnd();
    glFlush();
}

void drawBoundary(std::vector<Point2> pts) {
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < pts.size(); i++)
    {
        glVertex2f(pts[i].x, pts[i].y);
    }
    glEnd();
    glFlush();
}

std::vector<Point2> drawCircle(float cx, float cy, float r, int num_segments) 
{ 
    std::vector<Point2> pts;
	float theta = 2 * 3.1415926 / float(num_segments); 
	float tangetial_factor = tanf(theta);//calculate the tangential factor 

	float radial_factor = cosf(theta);//calculate the radial factor 
	
	float x = r;//we start at angle = 0 

	float y = 0; 
    
	glBegin(GL_LINE_LOOP); 
	for(int ii = 0; ii < num_segments; ii++) 
	{ 
		glVertex2f(x + cx, y + cy);//output vertex 
        pts.push_back(Point2(x + cx, y + cy));

        
		//calculate the tangential vector 
		//remember, the radial vector is (x, y) 
		//to get the tangential vector we flip those coordinates and negate one of them 

		float tx = -y; 
		float ty = x; 
        
		//add the tangential vector 

		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
        
		//correct using the radial factor 

		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd(); 
    return pts;
}

double Clip_line(const Point2& a, const Point2& b, const Point2 &c, const Point2 &d)
{
    if (inner(perp(d-c), b-a) >= 0 + EPSILON ||
            inner(perp(d-c), b-a) <= 0 - EPSILON)
    {
        double t = inner(perp(d-c), c-a)/inner(perp(d-c), b-a);
        double u = inner(perp(b-a), c-a)/inner(perp(d-c), b-a);
        // Ignore if t doesn't reach u, since we don't care
        if (t > 0.0001 && u >= 0.0000001 && u <= 1.0005)
        {
            return t;
        }
    }
    return -100000;
}

std::vector<Line> getLines(std::vector<std::vector<Point2>> polys) {
    std::vector<Line> lines;
    for (std::vector<Point2> poly : polys)
    {
        for (int i = 0; i < poly.size(); i++)
        {
            if (i + 1 == poly.size())
                lines.push_back(Line{poly[i], poly[0]});
            else
                lines.push_back(Line{poly[i], poly[i+1]});
        }
    }
    return lines;

}

void rayBouncer(Point2 a, Point2 b, std::vector<Line> lines)
{
    for (int j = 0; j < 1000; j++)
    {
        double minT = 10000;
        Point2 u, v;


        Point2 newB;
        for (Line line : lines)
        {
            Point2 p1 = line.a;
            Point2 p2 = line.b;
            double t = Clip_line(a, b, p1, p2);

            if (t == -100000)
                continue;

            if ( t < minT)
            {
                minT = t;
                u = p1;
                v = p2;
                newB  = a + (inner(perp(p2-p1), p1-a)/inner(perp(p2-p1), b-a)) * (b - a);
            }
        }
        
        b = newB;
        Vector2 rray = b - a;
        Vector2 re = reflect(rray, u-v);
        Point2 zed = Point2(b.x + re.delta_x(), b.y + re.delta_y());
        drawLine2(a, b);

        a = b;
        b = zed;
    }
}

void part1() {
    // Arbitrary Convex Polygon
    std::vector<Point2> p;
    p.push_back(Point2(50, 50));
    p.push_back(Point2(600, 50));
    p.push_back(Point2(600, 750));
    p.push_back(Point2(50, 500));
    
    // Draw the polygon
    drawBoundary(p);

    // Create vector of polygons to bounce against
    std::vector<std::vector<Point2>> aa;
    aa.push_back(p);

    // Initial line
    Point2 a, b;
    // arbitrary points
    a = { 250, 250};
    b = { 40, 135};

    // Bounce those lines baby
    rayBouncer(a, b, getLines(aa));
}

void display()
{
    part1();
    glutSwapBuffers();
}

void init(int *argc, char** argv) {
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Ray Bouncer");
    gluOrtho2D(0, 800, 0, 800);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glutDisplayFunc(display);
    glutMainLoop();
}

int main(int argc, char **argv) {
    init(&argc, argv);
}